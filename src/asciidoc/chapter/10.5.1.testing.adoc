[[spring-mvc-test-server-static-imports]]
====== Static Imports
The fluent API in the example above requires a few static imports such as
`MockMvcRequestBuilders.*`, `MockMvcResultMatchers.*`, and `MockMvcBuilders.*`. An easy
way to find these classes is to search for types matching __"MockMvc*"__. If using
Eclipse, be sure to add them as "favorite static members" in the Eclipse preferences
under__Java -> Editor -> Content Assist -> Favorites__. That will allow use of content
assist after typing the first character of the static method name. Other IDEs (e.g.
IntelliJ) may not require any additional configuration. Just check the support for code
completion on static members.

[[spring-mvc-test-server-setup-options]]
====== Setup Options
The goal of server-side test setup is to create an instance of `MockMvc` that can be
used to perform requests. There are two main options.

The first option is to point to Spring MVC configuration through the __TestContext
framework__, which loads the Spring configuration and injects a `WebApplicationContext`
into the test to use to create a `MockMvc`:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	@RunWith(SpringJUnit4ClassRunner.class)
	@WebAppConfiguration
	@ContextConfiguration("my-servlet-context.xml")
	public class MyWebTests {

		@Autowired
		private WebApplicationContext wac;

		private MockMvc mockMvc;

		@Before
		public void setup() {
			this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
		}

		// ...

	}
----

The second option is to simply register a controller instance without loading any Spring
configuration. Instead basic Spring MVC configuration suitable for testing annotated
controllers is automatically created. The created configuration is comparable to that of
the MVC JavaConfig (and the MVC namespace) and can be customized to a degree through
builder-style methods:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	public class MyWebTests {

		private MockMvc mockMvc;

		@Before
		public void setup() {
			this.mockMvc = MockMvcBuilders.standaloneSetup(new AccountController()).build();
		}

		// ...

	}
----

Which option should you use?

The __"webAppContextSetup"__ loads the actual Spring MVC configuration resulting in a
more complete integration test. Since the __TestContext framework__ caches the loaded
Spring configuration, it helps to keep tests running fast even as more tests get added.
Furthermore, you can inject mock services into controllers through Spring configuration,
in order to remain focused on testing the web layer. Here is an example of declaring a
mock service with Mockito:

[source,xml,indent=0]
[subs="verbatim,quotes"]
----
	<bean id="accountService" class="org.mockito.Mockito" factory-method="mock">
		<constructor-arg value="org.example.AccountService"/>
	</bean>
----

Then you can inject the mock service into the test in order set up and verify
expectations:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	@RunWith(SpringJUnit4ClassRunner.class)
	@WebAppConfiguration
	@ContextConfiguration("test-servlet-context.xml")
	public class AccountTests {

		@Autowired
		private WebApplicationContext wac;

		private MockMvc mockMvc;

		@Autowired
		private AccountService accountService;

		// ...

	}
----

The __"standaloneSetup"__ on the other hand is a little closer to a unit test. It tests
one controller at a time, the controller can be injected with mock dependencies
manually, and it doesn't involve loading Spring configuration. Such tests are more
focused in style and make it easier to see which controller is being tested, whether any
specific Spring MVC configuration is required to work, and so on. The "standaloneSetup"
is also a very convenient way to write ad-hoc tests to verify some behavior or to debug
an issue.

Just like with integration vs unit testing, there is no right or wrong answer. Using the
"standaloneSetup" does imply the need for some additional "webAppContextSetup" tests to
verify the Spring MVC configuration. Alternatively, you can decide write all tests with
"webAppContextSetup" and always test against actual Spring MVC configuration.

