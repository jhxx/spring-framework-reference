[[mvc-multipart]]
=== Spring's multipart (file upload) support



[[mvc-multipart-introduction]]
==== Introduction
Spring's built-in multipart support handles file uploads in web applications. You enable
this multipart support with pluggable `MultipartResolver` objects, defined in the
`org.springframework.web.multipart` package. Spring provides one `MultipartResolver`
implementation for use with http://jakarta.apache.org/commons/fileupload[__Commons
FileUpload__] and another for use with Servlet 3.0 multipart request parsing.

By default, Spring does no multipart handling, because some developers want to handle
multiparts themselves. You enable Spring multipart handling by adding a multipart
resolver to the web application's context. Each request is inspected to see if it
contains a multipart. If no multipart is found, the request continues as expected. If a
multipart is found in the request, the `MultipartResolver` that has been declared in
your context is used. After that, the multipart attribute in your request is treated
like any other attribute.



[[mvc-multipart-resolver-commons]]
==== Using a MultipartResolver with __Commons FileUpload__

The following example shows how to use the `CommonsMultipartResolver`:

[source,xml,indent=0]
[subs="verbatim,quotes"]
----
	<bean id="multipartResolver"
			class="org.springframework.web.multipart.commons.CommonsMultipartResolver">

		<!-- one of the properties available; the maximum file size in bytes -->
		<property name="maxUploadSize" value="100000"/>

	</bean>
----

Of course you also need to put the appropriate jars in your classpath for the multipart
resolver to work. In the case of the `CommonsMultipartResolver`, you need to use
`commons-fileupload.jar`.

When the Spring `DispatcherServlet` detects a multi-part request, it activates the
resolver that has been declared in your context and hands over the request. The resolver
then wraps the current `HttpServletRequest` into a `MultipartHttpServletRequest` that
supports multipart file uploads. Using the `MultipartHttpServletRequest`, you can get
information about the multiparts contained by this request and actually get access to
the multipart files themselves in your controllers.



[[mvc-multipart-resolver-standard]]
==== Using a MultipartResolver with __Servlet 3.0__

In order to use Servlet 3.0 based multipart parsing, you need to mark the
`DispatcherServlet` with a `"multipart-config"` section in `web.xml`, or with a
`javax.servlet.MultipartConfigElement` in programmatic Servlet registration, or in case
of a custom Servlet class possibly with a `javax.servlet.annotation.MultipartConfig`
annotation on your Servlet class. Configuration settings such as maximum sizes or
storage locations need to be applied at that Servlet registration level as Servlet 3.0
does not allow for those settings to be done from the MultipartResolver.

Once Servlet 3.0 multipart parsing has been enabled in one of the above mentioned ways
you can add the `StandardServletMultipartResolver` to your Spring configuration:

[source,xml,indent=0]
[subs="verbatim,quotes"]
----
	<bean id="multipartResolver"
			class="org.springframework.web.multipart.support.StandardServletMultipartResolver">
	</bean>
----



[[mvc-multipart-forms]]
==== Handling a file upload in a form
After the `MultipartResolver` completes its job, the request is processed like any
other. First, create a form with a file input that will allow the user to upload a form.
The encoding attribute ( `enctype="multipart/form-data"`) lets the browser know how to
encode the form as multipart request:

[source,xml,indent=0]
[subs="verbatim,quotes"]
----
	<html>
		<head>
			<title>Upload a file please</title>
		</head>
		<body>
			<h1>Please upload a file</h1>
			<form method="post" action="/form" enctype="multipart/form-data">
				<input type="text" name="name"/>
				<input type="file" name="file"/>
				<input type="submit"/>
			</form>
		</body>
	</html>
----

The next step is to create a controller that handles the file upload. This controller is
very similar to a <<mvc-ann-controller,normal annotated `@Controller`>>, except that we
use `MultipartHttpServletRequest` or `MultipartFile` in the method parameters:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	@Controller
	public class FileUploadController {

		@RequestMapping(value = "/form", method = RequestMethod.POST)
		public String handleFormUpload(@RequestParam("name") String name,
				@RequestParam("file") MultipartFile file) {

			if (!file.isEmpty()) {
				byte[] bytes = file.getBytes();
				// store the bytes somewhere
				return "redirect:uploadSuccess";
			}

			return "redirect:uploadFailure";
		}

	}
----

Note how the `@RequestParam` method parameters map to the input elements declared in the
form. In this example, nothing is done with the `byte[]`, but in practice you can save
it in a database, store it on the file system, and so on.

When using Servlet 3.0 multipart parsing you can also use `javax.servlet.http.Part` for
the method parameter:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	@Controller
	public class FileUploadController {

		@RequestMapping(value = "/form", method = RequestMethod.POST)
		public String handleFormUpload(@RequestParam("name") String name,
				@RequestParam("file") Part file) {

			InputStream inputStream = file.getInputStream();
			// store bytes from uploaded file somewhere

			return "redirect:uploadSuccess";
		}

	}
----



[[mvc-multipart-forms-non-browsers]]
==== Handling a file upload request from programmatic clients
Multipart requests can also be submitted from non-browser clients in a RESTful service
scenario. All of the above examples and configuration apply here as well. However,
unlike browsers that typically submit files and simple form fields, a programmatic
client can also send more complex data of a specific content type -- for example a
multipart request with a file and second part with JSON formatted data:

[literal]
[subs="verbatim,quotes"]
----
POST /someUrl
Content-Type: multipart/mixed

--edt7Tfrdusa7r3lNQc79vXuhIIMlatb7PQg7Vp
Content-Disposition: form-data; name="meta-data"
Content-Type: application/json; charset=UTF-8
Content-Transfer-Encoding: 8bit

{
	"name": "value"
}
--edt7Tfrdusa7r3lNQc79vXuhIIMlatb7PQg7Vp
Content-Disposition: form-data; name="file-data"; filename="file.properties"
Content-Type: text/xml
Content-Transfer-Encoding: 8bit
... File Data ...
----

You could access the part named "meta-data" with a `@RequestParam("meta-data") String
metadata` controller method argument. However, you would probably prefer to accept a
strongly typed object initialized from the JSON formatted data in the body of the
request part, very similar to the way `@RequestBody` converts the body of a
non-multipart request to a target object with the help of an `HttpMessageConverter`.

You can use the `@RequestPart` annotation instead of the `@RequestParam` annotation for
this purpose. It allows you to have the content of a specific multipart passed through
an `HttpMessageConverter` taking into consideration the `'Content-Type'` header of the
multipart:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	@RequestMapping(value="/someUrl", method = RequestMethod.POST)
	public String onSubmit(**@RequestPart("meta-data") MetaData metadata,
			@RequestPart("file-data") MultipartFile file**) {

		// ...

	}
----

Notice how `MultipartFile` method arguments can be accessed with `@RequestParam` or with
`@RequestPart` interchangeably. However, the `@RequestPart("meta-data") MetaData` method
argument in this case is read as JSON content based on its `'Content-Type'` header and
converted with the help of the `MappingJackson2HttpMessageConverter`.




